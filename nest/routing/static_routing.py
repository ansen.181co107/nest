"""Class to handle static routing related functionalities"""

from nest.topology_map import TopologyMap

class StaticRouting:
    """
    Handles Static Routing related fuctionalities
    """

    def __init__(self, hosts=None, routers=None, ldp_routers=None):
        """
        Constructor for StaticRouting

        Parameters
        ----------
        hosts: List[Node]
            List of hosts in the network. If `None`, considers the entire topology.
            Use this if your topology has disjoint networks
        routers: List[Node]
            List of routers in the network. If `None`, considers the entire topology.
            Use this if your topology has disjoint networks
        ldp_routers: List[Node]
            List of Routers which are to be used with mpls.
            Only enables ldp discovery on interfaces with mpls enabled
        """


        self.routers = TopologyMap.get_routers() if routers is None else routers
        self.hosts = TopologyMap.get_hosts() if hosts is None else hosts
        self.ldp_routers = ldp_routers if ldp_routers is not None else []

        self.interface_to_node_map = {}


        for host in self.hosts:
            for interface in host._interfaces:
                self.interface_to_node_map[interface.address] = host

        for router in self.routers:
            for interface in router._interfaces:
                self.interface_to_node_map[interface.address] = router


    def dfs(self, cur_node, dest_address, visited):
        """
        Run dfs for a given node and populate routing table entries keeping
        this node as destination. dfs also works if the topology has cycles,
        the algorithm will use a spanning tree for routing.

        Parameters
        ----------
        cur_node : Node
            The current node at which dfs is to be run
        start_node : Node
            The root node from which the dfs is run initially
        visited : Set(Node._id)
            The set of ids of all nodes visited during the dfs
        """

        for interface in cur_node._interfaces:
            interface_pair = interface._pair
            next_node = self.interface_to_node_map[interface_pair.address]

            if next_node._id not in visited:
                visited.add(next_node._id)
                next_node.add_route(
                    dest_addr=dest_address,
                    via_interface=interface_pair,
                    next_hop_addr=interface.address)
                self.dfs(next_node, dest_address, visited)

    def run_static_routing(self):
        """
        Iterate through all hosts and call dfs for them
        """

        for start_node in self.hosts:
            if len(start_node._interfaces) == 0:
                # a single host not connected to any router or host
                continue

            # it is assumed that a single host will be connected to only one single router
            self.dfs(
                cur_node=start_node,
                dest_address=start_node._interfaces[0].address,
                visited=set())
                